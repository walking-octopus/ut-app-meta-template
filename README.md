# Ubuntu Touch App Template

A template to generate apps for Ubuntu Touch.

## Usage

To get started with this template, simply run `clickable create`.

### OR

* Install the needed tools
    * [Cookiecutter](https://cookiecutter.readthedocs.io/en/latest/)
    * [clickable](https://clickable-ut.dev/en/latest/)
* Generate the project from this repo on Gitlab
    * Run `cookiecutter gl:clickable/ut-app-meta-template` to create a template
* Alternatively, clone this repo and generate the project from local repo
    * Run `git clone https://gitlab.com/clickable/ut-app-meta-template.git`
    * Make changes to the cloned meta template
    * Run `cookiecutter ut-app-meta-template` to create a template
* Go into the directory for your new project
* Run `clickable` to compile and install your new app!

## License

Copyright (C) 2020 [Brian Douglass](http://bhdouglass.com/)

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3, as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
